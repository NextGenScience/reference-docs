This is a document repository for next gen detectors.

See the [project wiki](https://git.ligo.org/NextGenScience/reference-docs/wikis/home) for more information.

----------
**To get started with git**
----------
----------

*Git global setup*

`git config --global user.name "Albert Einstein"
git config --global user.email "albert.einstein@ligo.org"`

----------

*Create a new repository*

`git clone git@git.ligo.org:NextGenScience/reference-docs.git
cd reference-docs
./gitsync`

----------

*Sync your repository with gitlab*

`./gitsync put a short comment here (no need for quotes)`
