% cost = costIFO(Nifo, Ltube, Dtube, Lsurface, ...
%                 Lflat, Nflat, Ltunnel, Hdepth, Ncavern)
%
% Nifo = number of interferometers (1 xylophone ET detector is 2 ifos)
% Ltube, Dtube = length of tube [km] with diameter Dtube [m]
% Lsurface = length of surface grading [km] (80 for CE)
% Lflat, Nflat = length and number of flat sections [km] (40, 2 for CE)
% Ltunnel = lentgh of tunnel [km] (30 for ET)
% Hdepth = depth of tunnel [m] (200 for ET)
% Ncavern = major halls (3 for ET triangle)
% 
% return is cost in MU (arbirary monetary units)
%
% CE example:
% costCE = costIFO(1, 80, 1.2, 80, 40, 2, 0, 0, 0)
%
% ET example:
% costET = costIFO(6, 120, 1.2, 0, 0, 0, 30, 200, 3)


function cost = costIFO(Nifo, Ltube, Dtube, Lsurface, ...
  Lflat, Nflat, Ltunnel, Hdepth, Ncavern)

  cost = 20 + 150 * Nifo + 40 * (Ltube / 10) * (Dtube / 1.2)^2 ...
    + 10 * (Lsurface / 10) + 1 * (Lflat / 10)^3 * Nflat ...
    + 100 * (Ltunnel / 10) ...
    + (30 + 10 * (Hdepth / 100)) * Ncavern;
end
